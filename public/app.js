const nextElem            = document.getElementById('next');
const imageDisplayElem    = document.getElementById('image-display');
const profileDisplayElem  = document.getElementById('profile-display');
const userNameElem        = document.getElementById('username');

nextElem.addEventListener('click', nextProfile);

let profiles;
let profileGenerator;

window.onload = firstProfile;

async function firstProfile() {
  await refillProfiles();
  nextProfile();
}

async function refillProfiles() {
  let data = await fetchProfiles(`https://randomuser.me/api/?results=5`);
  profiles = data.results;
  profileGenerator = createProfileGenerator(profiles);
}

async function fetchProfiles(url) {
  const data = await fetch(url)
    .then(response => {
      if (!response.ok)
        throw new Error("Response not OK!");
      return response.json();
    })
    .catch(error => console.log(`Network error: ${error.message}`));
  return data;
}

function nextProfile() {
  let profile = profileGenerator.next();
  if (profile.done) {
    // console.log(profile.value.name.first);
    renderNextProfile(profile.value);
    refillProfiles();
  } else {
    // console.log(profile.value.name.first);
    renderNextProfile(profile.value);
  }
}

function renderNextProfile(profile) {
  let imageSource = profile.picture.large;
  let name        = profile.name.first;
  let gender      = profile.gender;
  let age         = profile.dob.age;
  let username    = profile.login.username;
  let city        = profile.location.city;
  let state       = profile.location.state;
  let country     = profile.location.country;

  profileDisplayElem.innerHTML = `\
  <ul class="list-group">
    <li class="list-group-item">Name: ${name}</li>
    <li class="list-group-item">Gender: ${gender}</li>
    <li class="list-group-item">City: ${city} (${state})</li>
    <li class="list-group-item">Country: ${country}</li>
    <li class="list-group-item">Age: ${age}</li>
  </ul>`;
  imageDisplayElem.innerHTML = `<img src="${imageSource}">`;
  userNameElem.innerText = username;
  
}

function *createProfileGenerator(profiles) {
  let i = 0;
  while (i < profiles.length) {
    if (i == profiles.length - 1) {
      return profiles[i];
    } else {
      yield profiles[i];
    }
    i++;
  }
}


// console.log(`Name: ${name}\
//             \nGender: ${gender}\
//             \nAge: ${age}\
//             \nUsername: ${username}\
//             \nCity: ${city} (${state})\
//             \nCountry: ${country}`);
