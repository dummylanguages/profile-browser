# Profile Browser
A simple page that allows us to browse profiles from what could be some social network app. The goal was just to practice **ES6 Generators** (It uses no JS frameworks, just vanilla JavaScript).

Click [here](https://dummylanguages.gitlab.io/profile-browser/index.html) to see it live!

![Profile Browser Screenshot](profile_browser.png)

## Dependencies:

* [Bootstrap 5](https://getbootstrap.com)
* [Random User API](https://randomuser.me/)
